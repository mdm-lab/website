import { withoutTrailingSlash, withLeadingSlash, joinURL } from "ufo";


export function useWikiUrl(url: string | Ref<string>) {
    const wikiUrl = computed(() => {
        const sanitzedUrl = toValue(url)
        if (sanitzedUrl.startsWith("/") && !sanitzedUrl.startsWith("//")) {
            const _base = withLeadingSlash(
                withoutTrailingSlash(useRuntimeConfig().public.wikiUrl)
            );
            if (_base !== "/" && !sanitzedUrl.startsWith(_base)) {
                return joinURL(_base, sanitzedUrl);
            }
        }
        return toValue(url);
    });
    return { wikiUrl }
}