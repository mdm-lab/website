import { useSvgPlot } from './useSvgPlot';


export function useSerialize() {
    const xmlns = ref("http://www.w3.org/2000/xmlns/");
    const xlinkns = ref("http://www.w3.org/1999/xlink");
    const svgns = ref("http://www.w3.org/2000/svg");
    const blob = ref<Blob>()

    function serialize(compo: MaybeRef<ComponentPublicInstance | null>) {
        const toValueCompo = toValue(compo)

        if (toValueCompo !== null) {
            console.log(toValueCompo)
            const { svg } = useSvgPlot(toValueCompo)
            const toValueSvg = toValue(svg)
            if (toValueSvg !== null) {
                const clonedSvg = toValueSvg.cloneNode(true);
                const fragment = window.location.href + "#";
                const walker = document.createTreeWalker(toValueSvg, NodeFilter.SHOW_ELEMENT);
                while (walker.nextNode()) {
                    for (const attr of walker.currentNode.attributes) {
                        if (attr.value.includes(fragment)) {
                            attr.value = attr.value.replace(fragment, "#");
                        }
                    }
                }
                clonedSvg.setAttributeNS(xmlns.value, "xmlns", svgns.value);
                clonedSvg.setAttributeNS(xmlns.value, "xmlns:xlink", xlinkns.value);
                const serializer = new window.XMLSerializer;
                const string = serializer.serializeToString(clonedSvg);
                blob.value = new Blob([string], { type: "image/svg+xml" });
                return blob
            }
            else { return undefined }
        }
    }
    return { serialize }
}