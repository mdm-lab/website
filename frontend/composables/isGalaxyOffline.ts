import type { FetchError } from "ofetch"

export const useIsGalaxyOffline = ({ error }: {
  error: MaybeRef<Error | FetchError | null | undefined>
}) => {
  const message = ref<string>("The Galaxy instance used to run the analyses is currently offline. It is probably a maintenance ")
  const isGalaxyOffline = ref<boolean>(false)
  const toValError = toValue(error)
  console.dir(toValError)
  if (toValError && "data" in toValError) {
    if (toValError?.statusCode === 503 && toValError.data?.message === "The Galaxy instance is offline") {
      isGalaxyOffline.value = true
    }

  }

  return { message, isGalaxyOffline }
}
