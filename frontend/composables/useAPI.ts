import { useRuntimeConfig, type UseFetchOptions } from 'nuxt/app';
import { joinURL } from "ufo";

export function useAPI<T>(
    url: string | (() => string),
    options: UseFetchOptions<T> = {}
) {
    // change url if server side request on client-side
    const runtimeConfig = useRuntimeConfig()
    const sanitizedUrl = process.server
        ? joinURL(
            runtimeConfig.serverSideApiBaseUrl,
            runtimeConfig.dfApiPrefix,
            toValue(url)
        )
        : toValue(url)
    return useFetch(sanitizedUrl, {
        ...options,
        $fetch: useNuxtApp().$api,
    })
}