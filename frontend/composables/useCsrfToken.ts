import { useFetch } from "nuxt/app"


export function useCsrfToken() {
    const currentCsrfToken = useCookie('csrftoken')

    const csrfToken: Ref<string | undefined> = ref(undefined)
    const pending = ref()
    const error = ref()

    async function execute() {
        if (!currentCsrfToken.value) {
            const { data, error: errorCsrf, pending: pendingCsrf } = await useFetch<{ token: string }>(
                "/dfapi/analysis/csrf", {
                server: false,
            })
            if (data.value) {
                csrfToken.value = data.value.token
            }
            else {
                error.value = "No token"
            }
            error.value = errorCsrf
            pending.value = pendingCsrf
        } else {
            csrfToken.value = currentCsrfToken.value
        }
    }


    execute()
    return { csrfToken, pending, error }
}   