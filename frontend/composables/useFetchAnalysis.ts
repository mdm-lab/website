import { type Fn, useIntervalFn } from "@vueuse/core";
import { TERMINAL_STATE, type AnalysisPollingOptions, type JobState } from "~/types";


export function useAnalysisPolling(options: MaybeRef<AnalysisPollingOptions> = { interval: 5000, refresh: null, data: ref(null), pending: ref(false) }) {
    const terminalStates = computed(() => {
        return new Set<JobState>(TERMINAL_STATE)
    })

    const { interval, refresh, pending, data } = toValue(options)
    const { pause, resume, isActive } = useIntervalFn(() => {
        if (!pending.value) {
            console.log(
                `pending ${pending.value
                } | refreshing the data again ${new Date().toISOString()}`
            );
            if (refresh) refresh();

        }
    }, unref(interval));




    watchEffect(() => {
        if (data.value?.status !== undefined) {
            if (terminalStates.value.has(data.value.status) && pause != null) {
                pause();
            }
        }
    });
    return { pause, resume, isActive }

}