export function useSvgPlot(component: MaybeRef<ComponentPublicInstance>) {
    const svg = ref<SVGElement | null>(null)

    const toValueCompo = toValue(component)
    console.log(toValueCompo)
    const rootElem = toValueCompo
    console.log(rootElem)
    svg.value = rootElem.querySelector("svg")
    return { svg }
}