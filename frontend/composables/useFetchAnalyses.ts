import { useIntervalFn } from "@vueuse/core";
import { NON_TERMINAL_STATE, type JobState, type AnalysesPollingOptions, type Analysis } from "~/types";

export function usePolling(options: MaybeRef<AnalysesPollingOptions> = { interval: 5000, refresh: null, data: ref(null), pending: ref(false) }) {
    const nonTerminalState = computed(() => {
        return new Set<JobState>(NON_TERMINAL_STATE)
    })

    const { interval, refresh, pending, data } = toValue(options)
    const { pause, resume, isActive } = useIntervalFn(() => {
        if (!pending.value) {
            console.log(
                `pending ${pending.value
                } | refreshing the data again ${new Date().toISOString()}`
            );
            if (refresh) refresh();

        }
    }, unref(interval));

    watchEffect(() => {
        if (data.value && data.value?.length > 0) {
            let canPause = true
            for (const analysis of data.value) {
                if (nonTerminalState.value.has(analysis.status)) {
                    canPause = false
                }
            }
            if (canPause && pause !== null) pause()

        } else {
            pause()
        }

    });
    return { pause, resume, isActive }

}