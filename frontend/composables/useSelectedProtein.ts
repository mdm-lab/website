
const selectedProtein = ref<{ name: string | undefined, contig: string | undefined } | undefined>(undefined)

export function useSelectedProtein() {
    function setSelectedProtein(hit_id: string | undefined, contig: string | undefined) {
        selectedProtein.value = {
            name: hit_id, contig
        }

    }
    return { selectedProtein, setSelectedProtein }
}