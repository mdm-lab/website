export function useDownloadBlob() {
    function download(blob: MaybeRef<Blob>, filename: MaybeRef<string>) {
        const toValueBlob = toValue(blob)
        const toValueFilename = toValue(filename)
        var a = document.createElement("a");
        a.href = URL.createObjectURL(toValueBlob);
        a.download = toValueFilename;
        a.click();
        URL.revokeObjectURL(a.href);
    }

    return { download }

}