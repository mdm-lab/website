

export function useCssSelector(selector: MaybeRef<string>) {
    const sanitizedSelector = ref<string | undefined>(undefined)
    function sanitized(selector: MaybeRef<string>) {
        return toValue(selector).replace(/[^\w\d_-]/g, "-")
    }
    watchEffect(() => {
        sanitizedSelector.value = sanitized(selector)
    })

    return { sanitizedSelector }

}