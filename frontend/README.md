# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install
```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.


## Update package


clean node_modules and lock files

```bash
rm -rf node_modules
rm package-lock.json
```

Run fresh installation using docker image that match the one in Dockerfile

```bash
docker run -v "$PWD":/usr/src/app -w /usr/src/app --user 1000:1000 node:19.5-bullseye-slim npm install
```