
export default defineNuxtRouteMiddleware((to, from) => {
    const runtimeConfig = useRuntimeConfig()
    const isMaintenance = runtimeConfig?.public?.maintenance?.enabled === true
    
    if (!isMaintenance) return;

    if (isMaintenance && to.name !== "maintenance") {
        return navigateTo('/maintenance')
    }
})
