export default defineNuxtPlugin(() => {
    const headers = useRequestHeaders(['cookie'])
    const { csrfToken } = useCsrfToken()

    const $api = $fetch.create({
        baseURL: "/dfapi",
        async onRequest({ request, options, error }) {
            const { data } = await useFetch("/api/auth", { headers })
            const toValToken = toValue(csrfToken)
            if (toValToken !== undefined) {
                // Add Authorization header
                if (Array.isArray(options.headers)) {
                    options.headers = options.headers || []
                    options.headers = [...options.headers, ["X-CSRFToken", toValToken], ['cookie', data.value.cookies]]
                }
                else {
                    options.headers = options.headers || {}
                    options.headers = { ...options.headers, "X-CSRFToken": toValToken, cookie: data.value.cookies }
                }

            }
        },

    })
    // Expose to useNuxtApp().$api
    return {
        provide: {
            api: $api
        }
    }
})
