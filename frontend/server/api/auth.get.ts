import { useState } from "#app"
import { joinURL } from "ufo";

const runtimeConfig = useRuntimeConfig()
// const headers = useRequestHeaders(['cookie'])
// const sessionid = useCookie('sessionid')
// const headers = useRequestHeaders(['cookie'])
const sessionExpiryDate = useState('sessionExpiryDate')



export default defineEventHandler(async (event) => {
  const loginUrl = joinURL(
    runtimeConfig.serverSideApiBaseUrl,
    runtimeConfig.dfApiPrefix,
    '/analysis/login'
  )
  const headers = getRequestHeaders(event)
  const res = await $fetch.raw<{ session: 'ok', expire_date: string }>(loginUrl, { headers })
  sessionExpiryDate.value = Date.now()
  const cookies = (res.headers.get('set-cookie') || '')
  appendResponseHeader(event, 'set-cookie', cookies)
  return { cookies: headers.cookie }
})
