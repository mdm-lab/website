export const NON_TERMINAL_STATE = [
    "running",
] as const

export type NonTerminalState = typeof NON_TERMINAL_STATE[number]

export const TERMINAL_STATE = ["done", "error", "pause"] as const

export type TerminalJobState = typeof TERMINAL_STATE[number]

export type JobState = NonTerminalState | TerminalJobState





export interface Analysis {
    id: number;
    name: string;
    input_name: string;
    status: JobState;
    percentage_done: number;
    create_time: string
    from_nt: boolean
}





export interface Protein {
    id: string,
    length: number
    protLength: number
    strand: -1 | 1 | null,
    start: number | null
    end: number | null
    name?: string,
    defenseSystem?: string | undefined,
    isDefenseSystem?: boolean,
    isHmmerHit?: boolean

}

export interface ProteinsOut {
    analysis: number,
    proteins: Protein[]

}


// Genes
export interface Gene {
    replicon: string,
    hit_id: string,
    gene_name: string,
    hit_pos: number,
    model_fqn: string,
    sys_id: string,
    sys_loci: number,
    locus_num: number,
    sys_wholeness: number,
    sys_score: number,
    sys_occ: number,
    hit_gene_ref: string,
    hit_status: string,
    hit_seq_len: number,
    hit_i_eval: number,
    hit_score: number,
    hit_profile_cov: number,
    hit_seq_cov: number,
    hit_begin_match: number,
    hit_end_match: number,
    counterpart: string,
    used_in: string
}

export interface GenesOut {
    analysis: number,
    genes: Gene[]

}


// hmmers

export interface Hmmer {
    hit_id: string,
    replicon: string,
    hit_pos: number,
    hit_sequence_length: string,
    gene_name: string,
    i_eval: number,
    hit_score: number,
    hit_profile_cov: number,
    hit_seq_cov: number,
    hit_begin_match: number,
    hit_end_match: number
}

export interface HmmersOut {
    analysis: number,
    hmmers: Hmmer[]

}


export interface System {

    sys_id: string,
    type: string,
    subtype: string,
    sys_beg: string,
    sys_end: string,
    protein_in_syst: string,
    genes_count: number,
    name_of_profiles_in_sys: string

}

export interface SystemsOut {
    analysis: number,
    systems: System[]

}


export type RefreshSignature = (opts?: any) => Promise<Analysis[] | null>

export interface AnalysisPollingOptions {
    interval: number,
    refresh: RefreshSignature | null,
    data: Ref<Analysis | null>
    pending: Ref<boolean>

}

export interface AnalysesPollingOptions {
    interval: number,
    refresh: RefreshSignature | null,
    data: Ref<Analysis[] | null>
    pending: Ref<boolean>

}