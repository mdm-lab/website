// https://nuxt.com/docs/api/configuration/nuxt-config
import { md3 } from 'vuetify/blueprints'
import pkg from './package.json'

export default defineNuxtConfig({
  ssr: true,
  app: {
    head: {
      title: "Webservice"
    },
   
  },
  runtimeConfig: {
    serverSideApiBaseUrl: "http://defense-finder-api:8000",
    dfApiPrefix: "/dfapi",
    public: {
      wikiUrl: '/wiki',
      dfApiPrefix: "/dfapi",
      version: pkg.version,
      displayMessage: false,
      maintenance: {
        enabled: false,
        message: "The site is currently under maintenance. Please check back later.",
      },
    }
  },
  modules: [
    '@unocss/nuxt',
    'vuetify-nuxt-module',
    '@nuxtjs/plausible',
    "nuxt-security",
    '@nuxtjs/seo',
    // "nuxt-csurf",
  ],
  css: [
    '@vue-flow/core/dist/style.css',
    '@vue-flow/core/dist/theme-default.css',
    '@vue-flow/minimap/dist/style.css',
    '@vue-flow/controls/dist/style.css',
  ],

  site: {
    url: 'https://defensefinder.mdmlab.fr',
    name: 'DefenseFinder webservice and knowledge base',
    description: 'On this site, you can freely use (without any login) the DefenseFinder webservice (see below) and get help to navigate the ever expanding world of defense systems.There is a collaborative knowledge base of defense systems that provide essential information on any given defense systems',
    defaultLocale: 'en', // not needed if you have @nuxtjs/i18n installed
  },
  security: {
    headers: {
      contentSecurityPolicy: {
        "img-src": ["'self'", "data:", "blob:"]
      }
    }
  },
  vuetify: {
    vuetifyOptions: {
      labComponents: true,
      icons: {
        defaultSet: 'unocss-mdi',
        sets: ['mdi', 'fa', 'md'],
        unocssIcons: {
          // default is i-mdi:close-circle
          // database: 'i-tabler:database',
          // even from another collection, default is i-mdi:chevron-up
          // generalconcept: 'i-mdi:book-education-outline',
          // help: 'i-tabler:help'
        }
      },
      blueprint: md3


    }
  },
  plausible: {
    apiHost: 'https://plausible.pasteur.cloud',
    enableAutoOutboundTracking: true
  },
  devtools: {
    enabled: false
  },
  vite: {
    define: {
      "__VUE_PROD_HYDRATION_MISMATCH_DETAILS__": true,
    }
  },
  // devServer: {
  //   host: "http://defense-finder-api:8000",
  // },
  // experimental: {
  //   clientFallback: true,
  //   noVueServer: true,
  // }
})
