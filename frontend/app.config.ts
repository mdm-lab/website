export default defineAppConfig({
  defenseFinder: {
    software: {
      src: "https://github.com/mdmparis/defense-finder/releases/tag",
      version: "v2.0.0",
    },
    models: {
      src:"https://github.com/mdmparis/defense-finder-models/releases/tag",
      version: "2.0.2",
    }
  }
})