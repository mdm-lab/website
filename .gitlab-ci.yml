workflow:
   rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - when: always




stages:
  - delete-release
  - build
  - deploy

variables:
  DJANGO_NINJA_IMG_NAME: "django-ninja"
  NUXT_IMG_NAME: "nuxt"
  HELM_VERSION: "3.9.3"

.build:
  stage: build
  image: docker:24
  variables:
    CONTEXT: "."
  before_script:
    - i=0; while [ "$i" -lt 12 ]; do docker info && break; sleep 5; i=$(( i + 1 )) ; done
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_SHORT_SHA" --build-arg="BUILD_OPTIONS=$OPTIONS" -f $DOCKERFILE $CONTEXT
    - docker push "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_SHORT_SHA"

.build-nuxt:
  extends: .build
  script:
    - docker build --pull --target serve -t "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_SHORT_SHA" --build-arg="BUILD_OPTIONS=$OPTIONS" -f $DOCKERFILE $CONTEXT
    - docker push "$CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_SHORT_SHA"

.deploy:
  stage: deploy
  image: harbor.pasteur.fr/kube-system/helm-kubectl:$HELM_VERSION
  variables:
    CI_DEBUG_TRACE: "false"
    TEAM_ID: "df"
    DF_API_PREFIX: "dfapi"
    DISPLAY_MESSAGE: "false"
  script:
    - >
      helm upgrade --install $CI_PROJECT_NAME-$CI_ENVIRONMENT_NAME ./deploy/ --namespace=${KUBE_NAMESPACE}
      --set registry.image=${CI_REGISTRY_IMAGE}
      --set registry.username=${DEPLOY_USER}
      --set registry.password=${DEPLOY_TOKEN}
      --set registry.host=${CI_REGISTRY}
      --set imagePullSecrets[0].name="registry-pull-secret-${CI_COMMIT_REF_SLUG}"
      --set ingress.hosts[0].host="${PUBLIC_URL}"
      --set ingress.hosts[0].paths[0].path="/"
      --set postgresql.teamId=${TEAM_ID}
      --set djangoninja.imagePullSecrets[0].name="registry-pull-secret-${CI_COMMIT_REF_SLUG}"
      --set djangoninja.image.repository="$CI_REGISTRY_IMAGE/$DJANGO_NINJA_IMG_NAME"
      --set djangoninja.image.tag="$CI_COMMIT_SHORT_SHA"
      --set djangoninja.postgresql.teamId=${TEAM_ID}
      --set djangoninja.django.secret=${SECRET_KEY}
      --set djangoninja.galaxy.key=${GALAXY_API_KEY_DEV}
      --set djangoninja.galaxy.email="${GALAXY_EMAIL}"
      --set djangoninja.galaxy.workflowId=${GALAXY_WORKFLOW_ID}
      --set djangoninja.galaxy.instance="https://galaxy.pasteur.fr"
      --set djangoninja.galaxy.instanceName="Galaxy@Pasteur"
      --set djangoninja.galaxy.workflowName="defense-finder"
      --set djangoninja.apiPrefix="${DF_API_PREFIX}"
      --set nuxt.imagePullSecrets[0].name="registry-pull-secret-${CI_COMMIT_REF_SLUG}"      
      --set nuxt.image.repository="$CI_REGISTRY_IMAGE/$NUXT_IMG_NAME"
      --set nuxt.image.tag="$CI_COMMIT_SHORT_SHA"
      --set nuxt.dfApiPrefix="/${DF_API_PREFIX}"
      --set nuxt.displayMessage="${DISPLAY_MESSAGE}"
      --set env="$ENV"
      --values deploy/values.yaml
      --values deploy/values.${ENV:-development}.yaml

# test-style:
#   stage: test
#   needs: []
#   image: python:3.8
#   script:
#     - pip install -q -r requirements-dev.txt
#     - black ./backend/ --check --diff --config ./pyproject.toml


delete-helm-release:dev:
  rules:
    - if: $CI_COMMIT_BRANCH != "main"
  stage: delete-release
  when: manual
  image: harbor.pasteur.fr/kube-system/helm-kubectl:$HELM_VERSION
  variables:
    GIT_STRATEGY: none # important to not checkout source when branch is deleted
    NAMESPACE: "defense-finder-dev"
  environment:
    name: "k8sdev-01"
    action: stop
  script:
    - echo "Removing $CI_PROJECT_NAME-$CI_ENVIRONMENT_NAME"
    - helm delete -n ${NAMESPACE} $CI_PROJECT_NAME-$CI_ENVIRONMENT_NAME


delete-helm-release:prod:
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  stage: delete-release
  when: manual
  image: harbor.pasteur.fr/kube-system/helm-kubectl:$HELM_VERSION
  variables:
    GIT_STRATEGY: none # important to not checkout source when branch is deleted
    NAMESPACE: "defense-finder-prod"
  environment:
    name: "k8sprod-02"
    action: stop
  script:
    - echo "Removing $CI_PROJECT_NAME-$CI_ENVIRONMENT_NAME"
    - helm delete -n ${NAMESPACE} $CI_PROJECT_NAME-$CI_ENVIRONMENT_NAME


build:django-ninja:
  extends: .build
  variables:
    DOCKERFILE: "./backend/Dockerfile"
    IMAGE_NAME: ${DJANGO_NINJA_IMG_NAME}
    CONTEXT: "./backend"


build:nuxt:
  extends: .build-nuxt
  variables:
    DOCKERFILE: "./frontend/Dockerfile"
    IMAGE_NAME: "nuxt"
    CONTEXT: "./frontend"

deploy:dev:
  extends: .deploy
  rules:
    - if: $CI_COMMIT_BRANCH != "main"
  needs: ["build:django-ninja", "build:nuxt"]
  variables:
    NODE_ENV: "development"
    KUBE_NAMESPACE: "defense-finder-dev"
    PUBLIC_URL: "defense-finder.dev.pasteur.cloud"
    ENV: "development"
    DISPLAY_MESSAGE: "false"
    CI_DEBUG_TRACE: "true"
  environment:
    name: k8sdev-01
    url: "https://defense-finder.dev.pasteur.cloud"


deploy:prod:
  extends: .deploy
  when: manual
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  needs: ["build:django-ninja", "build:nuxt"]
  variables:
    NODE_ENV: "production"
    KUBE_NAMESPACE: "defense-finder-prod"
    # PUBLIC_URL: "defense-finder.pasteur.cloud"
    PUBLIC_URL: "defensefinder.mdmlab.fr"
    ENV: "production"
    CI_DEBUG_TRACE: "false"
    DISPLAY_MESSAGE: "false"

  environment:
    name: k8sprod-02
    url: "https://defense-finder.pasteur.cloud"

