FROM python:3.11.4-bookworm as requirements-stage

WORKDIR /tmp

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install poetry poetry-plugin-export

COPY ./pyproject.toml ./poetry.lock* /tmp/

RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

FROM python:3.11.4-slim-bookworm

RUN apt update -y && apt upgrade -y && apt install -y python3-dev libpq-dev

WORKDIR /code

COPY --from=requirements-stage /tmp/requirements.txt /code/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY . ./

EXPOSE 8000

CMD ["gunicorn", "--timeout", "120", "--bind", ":8000", "defense_finder_api.wsgi:application"]
