import csv
import tarfile
from typing import Any
from django.db import models
from django.contrib.sessions.models import Session
from tempfile import mkstemp
from django.conf import settings
from Bio import SeqIO
from pathlib import Path
from rich.console import Console
from typing import Dict


# from galaxy.models import WorkflowInvocation

from django_to_galaxy.models import (
    Invocation,
    Workflow,
    GalaxyUser,
    History as DgHistory,
)
from django_to_galaxy.models.invocation import DONE, ERROR
from django_to_galaxy.utils import (
    load_galaxy_invocation_time_to_datetime,
    load_galaxy_history_time_to_datetime,
)

from .schemas import GeneEntry, HmmerEntry, ProteinEntry, SystemEntry

# Create your models here.

console = Console()


class AnalysisGalaxyUser(GalaxyUser):
    def create_history(self, session, name=None):
        """Create history on the Galaxy instance."""
        name = self._generate_history_name() if name is None else name
        try:
            galaxy_history = self.obj_gi.histories.create(name=name)

            for tag in [
                settings.DF_HOSTNAME,
                session.session_key,
                session.expire_date.strftime("%Y-%m-%d_%H:%M:%S"),
            ]:
                # print(tag)
                self.obj_gi.gi.histories.create_history_tag(galaxy_history.id, tag)

            local_history = AnalysisHistory(
                galaxy_id=galaxy_history.id,
                name=galaxy_history.name,
                published=galaxy_history.published,
                galaxy_state=galaxy_history.state,
                create_time=load_galaxy_history_time_to_datetime(galaxy_history),
                galaxy_owner=self,
                analysis_owner=self,
                session=session,
            )
            local_history.save()
            return local_history
        except Exception as e:
            raise e


class AnalysisHistory(DgHistory):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    analysis_owner = models.ForeignKey(AnalysisGalaxyUser, on_delete=models.CASCADE)


class AnalysisWorkflow(Workflow):
    analysis_owner = models.ForeignKey(AnalysisGalaxyUser, on_delete=models.CASCADE)

    def invoke(self, session, input_files, params, history_name):
        history, datamap, params = self.prepare_invocation(
            session, input_files, params, history_name
        )
        print(params)
        galaxy_inv = self.galaxy_workflow.invoke(
            datamap, history=history.galaxy_history, params=params
        )

        try:
            analysis = Analysis(
                name=history_name,
                input_name=history_name,
                galaxy_id=galaxy_inv.id,
                galaxy_state=galaxy_inv.state,
                analysis_workflow=self,
                workflow=self,
                history=history,
                analysis_history=history,
                params=params,
                datamap=datamap,
                create_time=load_galaxy_invocation_time_to_datetime(galaxy_inv),
            )
        except ZeroDivisionError as e:
            print(e)
        finally:
            analysis.save()
            analysis.create_output_files(max_retry=20)
            return analysis

    def prepare_invocation(
        self,
        session,
        input_files: list[str],
        params: dict[str, dict[str, int | str]],
        history_name="defense-finder",
    ) -> tuple[Any, dict[str, object], dict[str, dict[str, int | str]]]:
        # create history
        history = self.analysis_owner.create_history(session, history_name)

        # upload files

        datamap = dict()
        for i, file_path in enumerate(input_files):
            upload_response = history.upload_file(file_path, file_name=history_name)
            datamap[i] = {"id": upload_response.id, "src": "hda"}
            self.analysis_owner.obj_gi.gi.histories.update_dataset(
                history.galaxy_id,
                upload_response.id,
                tags=["sequences"],
            )
        return history, datamap, params


class Analysis(Invocation):
    name = models.CharField(max_length=256)
    input_name = models.CharField(max_length=256)
    analysis_workflow = models.ForeignKey(AnalysisWorkflow, on_delete=models.CASCADE)
    analysis_history = models.ForeignKey(AnalysisHistory, on_delete=models.CASCADE)
    params = models.JSONField()
    datamap = models.JSONField()
    stderr = models.TextField(blank=True)
    from_nt = models.BooleanField(default=False)

    class Meta:
        ordering = ["-create_time"]

    def _build_job_id_to_tools(self) -> Dict[str, dict]:
        step_jobs_summary = self.galaxy_invocation.step_jobs_summary()
        job_id_to_tools = {}
        for step in step_jobs_summary:
            job_id = step["id"]
            job = self.workflow.galaxy_owner.obj_gi.jobs.get(job_id)
            wrapped_tool = self.workflow.galaxy_owner.obj_gi.tools.get(
                job.wrapped["tool_id"]
            ).wrapped
            job_id_to_tools[job_id] = wrapped_tool
        return job_id_to_tools

        """Retrive `step_jobs_summary` with details of tool used."""
        step_jobs_summary = self.galaxy_invocation.step_jobs_summary()
        detailed_jobs_summary = []
        for step in step_jobs_summary:
            detailed_step = step
            job_id = step["id"]
            detailed_step["tool"] = self.job_id_to_tools_perso.get(job_id, {})
            detailed_jobs_summary.append(detailed_step)
        return detailed_jobs_summary

    def download_results(
        self,
    ):
        paths: list = []
        fd, archive_name = mkstemp(
            prefix=f"defense-finder-analysis-{self.id}-results-",
            suffix=".tar.gz",
            dir="/uploaded-files",
        )
        if self.status == DONE:
            paths.append(self.load_dataset("genes", "genes"))
            paths.append(self.load_dataset("systems", "systems"))
            paths.append(self.load_dataset("hmmers", "hmmers"))

            prot_file_path = self.load_dataset("proteins", "proteins", ext="faa")
            proteins = self.read_fasta_file(prot_file_path)
            if len(proteins) <= 1:
                prot_file_path = self.load_dataset("sequences", "proteins", ext="faa")
            paths.append(prot_file_path)
            with tarfile.open(archive_name, "w:gz") as tar:
                for path in paths:
                    tar.add(path, arcname=Path(path).name)

        return archive_name

    def load_dataset(self, pattern: str, type: str, ext: str = "tsv"):
        galaxy_id = self.analysis_history.galaxy_id

        galaxy_history_datasets = (
            self.analysis_history.galaxy_owner.obj_gi.histories.get(
                galaxy_id
            ).content_infos
        )
        # print(galaxy_history_datasets)
        for dataset in galaxy_history_datasets:
            if pattern in dataset.wrapped["tags"]:
                dataset_id = dataset.id
                fd, file_path = mkstemp(
                    prefix=f"defense-finder-{type}-{dataset_id}-",
                    suffix=f".{ext}",
                    dir="/uploaded-files",
                )
                self.analysis_history.galaxy_owner.obj_gi.gi.datasets.download_dataset(
                    dataset_id, file_path=file_path, use_default_filename=False
                )
                return file_path
        return None

    def load_genes(self):
        queryset = Gene.objects.filter(analysis=self)

        if self.status == DONE and queryset.count() == 0:
            genes_path = self.load_dataset("genes", "genes")
            if genes_path is not None:
                with open(genes_path, newline="") as csvfile:
                    reader = csv.reader(csvfile, delimiter="\t")
                    # skip the reader
                    headers = next(reader, None)
                    if headers:
                        json_genes = [
                            GeneEntry(**dict(zip(headers, gene))).dict()
                            for gene in reader
                        ]
                        genes = Gene(analysis=self, genes=json_genes)
                        genes.save()

    def load_systems(self):
        queryset = System.objects.filter(analysis=self)

        if self.status == DONE and queryset.count() == 0:
            systems_path = self.load_dataset("systems", "systems")
            if systems_path is not None:
                with open(systems_path, newline="") as csvfile:
                    reader = csv.reader(csvfile, delimiter="\t")
                    # skip the reader
                    headers = next(reader, None)
                    if headers:
                        entries = [
                            SystemEntry(**dict(zip(headers, entry))).dict()
                            for entry in reader
                        ]
                        instance = System(analysis=self, systems=entries)
                        instance.save()

    def load_hmmers(self):
        queryset = Hmmer.objects.filter(analysis=self)
        if self.status == DONE and queryset.count() == 0:
            hmmers_path = self.load_dataset("hmmers", "hmmers")
            if hmmers_path is not None:
                with open(hmmers_path, newline="") as csvfile:
                    reader = csv.reader(csvfile, delimiter="\t")
                    # skip the reader
                    headers = next(reader, None)
                    if headers:
                        entries = [
                            HmmerEntry(**dict(zip(headers, entry))).dict()
                            for entry in reader
                        ]
                        instance = Hmmer(analysis=self, hmmers=entries)
                        instance.save()

    def load_proteins(self):
        queryset = Protein.objects.filter(analysis=self)
        if self.status == DONE and queryset.count() == 0:
            file_path = self.load_dataset("proteins", "proteins")
            proteins = self.read_fasta_file(file_path, isFromNt=True)
            if len(proteins) <= 1:
                file_path = self.load_dataset("sequences", "proteins")
                proteins = self.read_fasta_file(file_path)
            json_prots = [ProteinEntry(**prot).dict() for prot in proteins]
            # print(json_prots)
            prots = Protein(analysis=self, proteins=json_prots)
            prots.save()

    def set_stderr(self):

        if self.status == ERROR and self.stderr == "":
            steps = self.detailed_step_jobs_summary
            for step in steps:
                job_id = step["id"]
                job = self.analysis_history.analysis_owner.obj_gi.jobs.get(
                    job_id, full_details=True
                )
                self.stderr = job.wrapped["tool_stderr"]
                self.save()

    def read_fasta_file(self, file_path, isFromNt=False):
        self.from_nt = isFromNt
        self.save()
        # if is from Nt, need to sum prot length.
        # In order to get proteins that belongs to same contig
        # just remove (_\d+) to the id

        sequences = []
        if file_path is not None:
            with open(file_path) as handle:
                # current_contig = None
                # offset = 0
                # last_prot_end = 0
                for record in SeqIO.parse(handle, "fasta"):

                    prot = {"id": record.id, "length": len(record), "strand": None}
                    # print(len(record))
                    # if isFromNt:
                    #     contig = "-".join(prot["id"].split("_")[0:-1])
                    #     if current_contig is None or contig != current_contig:
                    #         # print(contig)

                    #         current_contig = contig
                    #         if current_contig is not None:
                    #             # new_offset = int(last_prot_end)
                    #             # offset = new_offset
                    #             if last_prot_end > 99999999:
                    #                 return sequences

                    description_list = record.description.split(" # ")

                    if len(description_list) == 5:
                        start = int(description_list[1])
                        end = int(description_list[2])
                        strand = int(description_list[3])

                        if strand == 1 or strand == -1:
                            prot["strand"] = strand
                        else:
                            strand = None
                        # if isFromNt:
                        #     prot["start"] = offset + start
                        #     prot["end"] = offset + end
                        #     last_prot_end = int(prot["end"])

                        # else:
                        prot["start"] = start
                        prot["end"] = end
                    sequences.append(prot)
        return sequences


class Protein(models.Model):
    analysis = models.OneToOneField(Analysis, on_delete=models.CASCADE)
    proteins = models.JSONField()


class Gene(models.Model):
    analysis = models.OneToOneField(Analysis, on_delete=models.CASCADE)
    genes = models.JSONField()


class System(models.Model):
    analysis = models.OneToOneField(Analysis, on_delete=models.CASCADE)
    systems = models.JSONField()


class Hmmer(models.Model):
    analysis = models.OneToOneField(Analysis, on_delete=models.CASCADE)
    hmmers = models.JSONField()
