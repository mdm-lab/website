from django.core.management.base import BaseCommand
from django_to_galaxy.models import GalaxyInstance
from analysis.models import AnalysisWorkflow, AnalysisGalaxyUser
from django.conf import settings


class Command(BaseCommand):
    help = "add default conf"

    def handle(self, *args, **options):
        gi, _ = GalaxyInstance.objects.get_or_create(
            url=settings.GALAXY_INSTANCE, name=settings.GALAXY_INSTANCE_NAME
        )
        gu, _ = AnalysisGalaxyUser.objects.get_or_create(
            email=settings.GALAXY_EMAIL,
            api_key=settings.GALAXY_API_KEY,
            galaxy_instance=gi,
        )
        print(gu)

        gw, _ = AnalysisWorkflow.objects.get_or_create(
            galaxy_id=settings.GALAXY_WORKFLOW_ID,
            galaxy_owner=gu,
            analysis_owner=gu,
            name=settings.GALAXY_WORKFLOW_NAME,
            published=False,
        )
        print(gw)
