from typing import Optional
from ninja import Schema


class GeneEntry(Schema):
    replicon: str
    activity: Optional[str]
    hit_id: str
    gene_name: str
    hit_pos: int
    model_fqn: str
    sys_id: str
    sys_loci: int
    locus_num: int
    sys_wholeness: float
    sys_score: float
    sys_occ: int
    hit_gene_ref: str
    hit_status: str
    hit_seq_len: int
    hit_i_eval: float
    hit_score: float
    hit_profile_cov: float
    hit_seq_cov: float
    hit_begin_match: int
    hit_end_match: int
    counterpart: str
    used_in: str


class SystemEntry(Schema):
    sys_id: str
    type: str
    activity: Optional[str]
    subtype: str
    sys_beg: str
    sys_end: str
    protein_in_syst: str
    genes_count: int
    name_of_profiles_in_sys: str


class HmmerEntry(Schema):
    hit_id: str
    replicon: str
    hit_pos: int
    hit_sequence_length: str
    gene_name: str
    i_eval: float
    hit_score: float
    hit_profile_cov: float
    hit_seq_cov: float
    hit_begin_match: int
    hit_end_match: int


class ProteinEntry(Schema):
    id: str
    length: int
    strand: Optional[int]
    start: Optional[int]
    end: Optional[int]