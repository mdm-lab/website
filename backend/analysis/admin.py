from django.contrib import admin

# Register your models here.
from django_to_galaxy.admin import HistoryAdmin

from analysis.models import AnalysisHistory


@admin.register(AnalysisHistory)
class AnalysisHistoryAdmin(HistoryAdmin):
    pass
