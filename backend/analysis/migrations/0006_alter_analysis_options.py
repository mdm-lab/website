# Generated by Django 4.2.3 on 2023-11-20 12:22

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("analysis", "0005_alter_analysis_options"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="analysis",
            options={"ordering": ["-create_time"]},
        ),
    ]
