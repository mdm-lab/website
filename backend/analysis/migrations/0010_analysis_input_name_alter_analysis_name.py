# Generated by Django 4.2.3 on 2024-07-01 13:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0009_analysis_from_nt'),
    ]

    operations = [
        migrations.AddField(
            model_name='analysis',
            name='input_name',
            field=models.CharField(default='to_change', max_length=256),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='analysis',
            name='name',
            field=models.CharField(max_length=256),
        ),
    ]
