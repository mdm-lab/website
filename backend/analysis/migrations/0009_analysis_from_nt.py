# Generated by Django 4.2.3 on 2024-06-28 08:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0008_analysis_stderr'),
    ]

    operations = [
        migrations.AddField(
            model_name='analysis',
            name='from_nt',
            field=models.BooleanField(default=False),
        ),
    ]
