from ninja import ModelSchema, Schema
from typing import List

from analysis.models import (
    Analysis,
    AnalysisWorkflow,
    AnalysisHistory,
    Gene,
    Hmmer,
    Protein,
    System,
)
from analysis.schemas import GeneEntry, HmmerEntry, ProteinEntry, SystemEntry


class AnalysisHistorySchema(ModelSchema):
    class Config:
        model = AnalysisHistory
        model_fields = [
            "id",
            "galaxy_id",
            "galaxy_owner",
            "galaxy_state",
            "tags",
            "published",
            "create_time",
        ]


class AnalysisWorkflowSchema(ModelSchema):
    class Config:
        model = AnalysisWorkflow
        model_fields = [
            "id",
            "galaxy_id",
            "analysis_owner",
            "annotation",
            "tags",
            "published",
        ]
        # model_exclude = ["galaxy_workflow"]


class AnalysisInSchema(ModelSchema):
    class Config:
        model = Analysis
        model_fields = ["name"]


class AnalysisOutSchema(ModelSchema):
    analysis_workflow: AnalysisWorkflowSchema
    history: AnalysisHistorySchema
    percentage_done: float = 0.0
    stderr: str

    class Config:
        model = Analysis
        model_exclude = ["workflow"]


class Error(Schema):
    message: str


class AnalysisPayload(Schema):
    antidefensefinder: bool


class GeneOutSchema(ModelSchema):
    genes: List[GeneEntry]

    class Config:
        model = Gene
        model_fields = ["analysis", "genes"]


class SystemOutSchema(ModelSchema):
    systems: List[SystemEntry]

    class Config:
        model = System
        model_fields = ["analysis", "systems"]


class HmmerOutSchema(ModelSchema):
    hmmers: List[HmmerEntry]

    class Config:
        model = Hmmer
        model_fields = ["analysis", "hmmers"]


class ProteinOutSchema(ModelSchema):
    proteins: List[ProteinEntry]

    class Config:
        model = Protein
        model_fields = ["analysis", "proteins"]
